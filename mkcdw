#!/bin/bash
#
# Create new directories specified on the command line and open a new xterm
# running a shell in each.
#
# Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
# 
# 2023-03-11 Initial creation. Extracted from cdw.
# 2023-03-13 Place under the Apache License, Version 2.0.
#

MYANME=`basename $0`

usage()
{
    echo "${MYNAME}"
    echo
    echo "Usage: ${MYNAME} <dir1> [<dir2> ...]"
    echo
    echo "Create the specified directories with mkdir -p and open an xterm running a shell in each."
    echo
}

create_open_dirs()
{
    for i in $@
    do
        if ! mkdir -p "${i}"
        then
            echo "Failed to create/find ${i} or or not a directory."
        else
            pushd "${i}" > /dev/null
            TITLE=`pwd`
            xterm -geometry ${GEOMETRY} ${COLOURS} -title ${TITLE} -e "${SHELL}" &
            disown %1
            popd > /dev/null
            # Sleep here for slow network connections
            # usleep 100000
        fi
    done
}

#
# Main
#
SHELL=${SHELL:-/bin/bash}
GEOMETRY="${GEOMETRY:-80x24}"
COLOURS="${COLOURS:--fg white -bg black}"

if [ $# -lt 1 ]
then
    usage
    exit 1
fi

create_open_dirs $@

