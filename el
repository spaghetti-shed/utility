#!/bin/bash
#
# Open the specified files in an editor (vim) in an xterm.
# Copyright (C) 2014-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
# 
# 2014-06-05 Initial creation.
# 2017-06-02 Add colour scheme.
# 2023-03-11 Place under the Apache License, Version 2.0.
#

MYANME=`basename $0`

usage()
{
    echo "${MYNAME}"
    echo
    echo "Usage: ${MYNAME} <file1> [<file2> ...]"
    echo
    echo "Open each file in vi (or \$EDITOR) in a separate xterm."
    echo
}


open_files()
{
    echo "open_files $@"
    for i in $@
    do
        if [ ! -f ${i} ]
        then
            echo "${i} not found."
        else
            RUNSTR="${EDITOR} ${i}"
            TITLE=`basename ${i}`
            xterm -geometry ${GEOMETRY} ${COLOURS} -title ${TITLE} -e ${RUNSTR} &
            disown %1
            # Sleep here for slow network connections
            # usleep 100000
        fi
    done
}

#
# Main
#
EDITOR="${EDITOR:-/usr/bin/vi}"
GEOMETRY="${GEOMETRY:-80x24}"
COLOURS="${COLOURS:--fg white -bg black}"

if [ $# -lt 1 ]
then
    usage
    exit 1
fi

open_files $@

