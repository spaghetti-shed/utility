Utility Scripts
===============

Very small utility scripts written in bash for personal productivity under
Slackware Linux.

Copyright (C) 2009-2023 by Iain Nicholson. iain.j.nicholson@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Introduction
------------

These scripts are very small and perform very specific jobs to make using the
command line more productive. Some examples of how to automate various
activities on the network are also provided.

Utility Scripts
---------------

* cdw: Open new xterms running a shell in each of the specified directories.
* datestamp:  Echo a datestamp of the form YYYYMMDD-hhmmss to stdout.
* el: Edit the list of files specified using separate instances of vim each in their own xterm.
* nospaces: Replace spaces in a filename with underscores (renames file).
* openall: For programming, open all the .c, .cpp, .java and .h files in the current directory plus any Makefiles in separate vim instances in separate xterms.
* terms: Open the specified number of xterms, running a shell, on the current host.

Networking Scripts
------------------

* isalive: Check whether the specified host is alive (reachable on the network) by sending a single ping packet and report "up" or "down" on stdout accordingly.
* monitor: For each network host specified, open an xterm running a shell running top in a loop to provide live status.
* nterms: Open the specified number of xterms on the specified remote host over ssh using public/private keys.
* hosts: Script to print out a list of names of hosts on my LAN as an example.  It can be used in conjunction with monitor (above):
        ``monitor `hosts` ``

