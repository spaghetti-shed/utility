#!/bin/bash
#
# Open an xterm running a shell in each of the specified directories.
#
# Copyright 2014-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
# 
# 2014-09-24 Initial creation.
# 2017-06-02 Add colour scheme.
# 2020-12-07 Improve and simply open_dirs() to print a useful dir name.
# 2023-03-11 Place under the Apache License, Version 2.0.
#

MYANME=`basename $0`

usage()
{
    echo "${MYNAME}"
    echo
    echo "Usage: ${MYNAME} <dir1> [<dir2> ...]"
    echo
    echo "Open an xterm running a shell in each specified directory."
    echo
}

open_dirs()
{
    for i in $@
    do
        if [ ! -d ${i} ]
        then
            echo "${i} not found or not a directory."
        else
            pushd "${i}" > /dev/null
            TITLE=`pwd`
            xterm -geometry ${GEOMETRY} ${COLOURS} -title ${TITLE} -e "${SHELL}" &
            disown %1
            popd > /dev/null
            # Sleep here for slow network connections
            # usleep 100000
        fi
    done
}

#
# Main
#
SHELL=${SHELL:-/bin/bash}
GEOMETRY=${GEOMETRY:-80x24}
COLOURS="${COLOURS:--fg white -bg black}"

if [ $# -lt 1 ]
then
    usage
    exit 1
fi

open_dirs $@

